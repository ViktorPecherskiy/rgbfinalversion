//
//  main.m
//  RGBColor
//
//  Created by Sveta on 3/20/18.
//  Copyright © 2018 Viktor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
  }
}
