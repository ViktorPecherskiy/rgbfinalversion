//
//  ObjCViewController.m
//  RGBColor
//
//  Created by Sveta on 3/21/18.
//  Copyright © 2018 Viktor. All rights reserved.
//

#import "ObjCViewController.h"

@interface ObjCViewController ()

@end

@implementation ObjCViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (IBAction)sliderChanged:(id)sender {
  NSLog(@"Red: %f" "Green: %f" "Blue: %f", self.sliderRed.value, self.sliderGreen.value, self.sliderBlue.value);
  [self updateView];
}

- (void) updateView { // проба пера для комита
  self.redLabel.text = [NSString stringWithFormat:@"%.2f", self.sliderRed.value];
   self.greenLabel.text = [NSString stringWithFormat:@"%.2f", self.sliderGreen.value];
   self.blueLabel.text = [NSString stringWithFormat:@"%.2f", self.sliderBlue.value];
  
    self.colorView.backgroundColor = [UIColor colorWithRed:self.sliderRed.value/255.0 green:self.sliderGreen.value/255.0 blue:self.sliderBlue.value/255.0 alpha:1];
  }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
