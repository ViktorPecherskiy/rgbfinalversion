//
//  AppDelegate.h
//  RGBColor
//
//  Created by Sveta on 3/20/18.
//  Copyright © 2018 Viktor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

