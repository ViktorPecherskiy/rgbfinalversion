//
//  ObjCViewController.h
//  RGBColor
//
//  Created by Sveta on 3/21/18.
//  Copyright © 2018 Viktor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ObjCViewController : UIViewController
@property (weak, nonatomic) IBOutlet UISlider *sliderRed;
@property (weak, nonatomic) IBOutlet UISlider *sliderGreen;
@property (weak, nonatomic) IBOutlet UISlider *sliderBlue;
@property (weak, nonatomic) IBOutlet UILabel *redLabel;
@property (weak, nonatomic) IBOutlet UILabel *greenLabel;
@property (weak, nonatomic) IBOutlet UILabel *blueLabel;
@property (weak, nonatomic) IBOutlet UIView *colorView;
- (IBAction)sliderChanged:(id)sender;





@end
